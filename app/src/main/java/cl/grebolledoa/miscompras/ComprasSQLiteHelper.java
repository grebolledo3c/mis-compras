package cl.grebolledoa.miscompras;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ComprasSQLiteHelper extends SQLiteOpenHelper {

    //definimos nuestra sentencia de creación de nuestra base de datos
    private String sqlCreate = "CREATE TABLE compras(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "descripcion TEXT NOT NULL," +
            "total NUMERICO NOT NULL"+
            ");";

    public ComprasSQLiteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //ejecutamos la sentencia de creación de la base de datos
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //se elimina la versión anterior de la base de datos
        db.execSQL("DROP TABLE IF EXISTS compras");

        db.execSQL(sqlCreate);
    }
}
