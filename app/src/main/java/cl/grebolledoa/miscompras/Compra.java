package cl.grebolledoa.miscompras;

public class Compra {

    public Compra(int id, String descripcion, double total){
        this.id  = id;
        this.descripcion = descripcion;
        this.total = total;
    }

    private int id;
    private String descripcion;
    private double total;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Compra {" +
                "id=" + id +
                ", descripcion='" + descripcion + '\'' +
                ", total=" + total +
                '}';
    }
}
