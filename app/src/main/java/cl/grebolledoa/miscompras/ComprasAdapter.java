package cl.grebolledoa.miscompras;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ComprasAdapter extends RecyclerView.Adapter<ComprasAdapter.ViewHolder>{

    private List<Compra> datos;
    private int layout;

    public ComprasAdapter(List<Compra> datos, int layout) {
        this.datos = datos;
        this.layout = layout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.datos.get(position));
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvDescripcion;
        private TextView tvTotal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tvDescripcion = itemView.findViewById(R.id.tv_descripcion);
            this.tvTotal = itemView.findViewById(R.id.tv_total);
        }

        private void bind(Compra compra){
            this.tvDescripcion.setText(compra.getDescripcion());
            String str = String.format("%,d", (int) compra.getTotal());
            str = str.replace(",",".");
            this.tvTotal.setText("$" + str);
        }
    }
}

