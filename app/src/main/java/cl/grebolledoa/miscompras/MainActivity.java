package cl.grebolledoa.miscompras;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ComprasSQLiteHelper comprasHelper;
    private SQLiteDatabase db;

    private List<Compra> datos = new ArrayList<>();

    private Button btn, btnLimpiar;
    private RecyclerView rcCompras;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btn = findViewById(R.id.bt_nuevo);
        this.btnLimpiar = findViewById(R.id.btn_limpiar);
        this.rcCompras = findViewById(R.id.rc_compras);

        //por medio del layout manager le decimos al recycler view como se vera
        this.layoutManager = new LinearLayoutManager(this);

        //asignamos el layout manager al recyclerview
        this.rcCompras.setLayoutManager(this.layoutManager);

        //abrimos nuestra base de datos 'schema_compras' en modo escritura
        this.comprasHelper = new ComprasSQLiteHelper(this, "schema_compras", null, 1);
        this.db = this.comprasHelper.getWritableDatabase();

        //obtenemos los datos en la base de datos
        getCompras();
        //creamos el adapter
        this.adapter = new ComprasAdapter(this.datos, R.layout.recycler_view_item);
        this.rcCompras.setAdapter(this.adapter);

        this.btn.setOnClickListener(v -> {
            this.create();
            //le notificamos al adapter que los datos se modificaron
            this.adapter.notifyDataSetChanged();
        });

        this.btnLimpiar.setOnClickListener(v -> {
            this.delete();
            //le notificamos al adapter que los datos se modificaron
            this.adapter.notifyDataSetChanged();
        });

    }

    private void getCompras()
    {
        //hacemos un select a la tabla compras y la almacenamos en un cursor
        Cursor cursor = db.rawQuery("SELECT * FROM compras", null);

        //validamos que tenga se pueda mover a la primera posición del cursor
        if(cursor.moveToFirst()){
            //limpiamos los datos de la lista
            this.datos.clear();
            while (!cursor.isAfterLast())
            {
                //obtenemos el indice de la columna "ID"
                int indexColumnId = cursor.getColumnIndex("id");
                //obtenemos un dato entero dada la posición de la columna
                int id = cursor.getInt(indexColumnId);
                //obtenemos la columna descripcion
                String descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));
                //obtenemos la columna total
                double total = cursor.getDouble(cursor.getColumnIndex("total"));

                //creamos el objeto de tipo compra
                Compra compra = new Compra(id, descripcion, total);

                //imprimo el objeto
                Log.i("Compra", compra.toString());

                //lo agregamos a la lista
                this.datos.add(compra);

                //movemos el cursor a la siguiente posicion
                cursor.moveToNext();
            }
        }else{
            //limpiamos los datos de la lista
            this.datos.clear();
            Log.w("Compra", "No existen compras");
        }

    }

    private void delete(){
        if(db != null){
            db.execSQL("DELETE FROM compras");
            getCompras();
        }
    }

    private void create(){
        //comprobamos que hemos abierto correctamente la base de datos
        if(db != null){
            //creamos el registro como un Content values
            ContentValues contentValues = new ContentValues();
            //la columna id no la insertamos dado que es autoincrement
            contentValues.put("descripcion", "Netflix");
            int random = (int) (Math.random() * (15000 - 10000)) + 10000;
            contentValues.put("total", random);
            long result = db.insert("compras", null, contentValues);

            if (result != -1){
                getCompras();
            }
        }
    }

    @Override
    public void onDestroy(){
        //cuando se destruya la activida cerramos la conexión a la base de datos
        db.close();
        super.onDestroy();
    }


}